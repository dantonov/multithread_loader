//
//
//

#include "server_task.h"

#include <unistd.h>
#include <sstream>

#include <protocol.h>

ServerTask::ServerTask(int clientSocketId, const std::string & homeDirectory)
	: mClientSocketId(clientSocketId),
	  mHomeDirectory(homeDirectory)
{
	mFileData.reset(nullptr);
}

ServerTask::~ServerTask() throw()
{
	if (mClientSocketId != -1)
		close(mClientSocketId);
	if (mPFile)
		fclose(mPFile);
}

void ServerTask::start()
{
	if (mClientSocketId < 0)
		return;

	printf("Task started\n");
	// A double max buffer to save previous command data in needed
	uint8_t buffer[sMaxBufferSize * 2];
	// Offset of the buffer to write to
	ssize_t currentOffset = 0;

	while (true)
	{
		// Read bytes from socket to fill the packet
		ssize_t bytesTotal = read(mClientSocketId, (void *)(buffer + currentOffset), sMaxBufferSize - currentOffset);
		if (bytesTotal < 0)
		{
			printf("Connection interupted\n");
			break;
		}
		bytesTotal += currentOffset;

		currentOffset = 0; // Reload current offset for future
		// Current patket read offset
		ssize_t readOffset = 0;
		// Reading packet structure
		while (bytesTotal > readOffset)
		{
			ssize_t restData = bytesTotal - readOffset;
			ssize_t bytesProcessed = parseCommand(buffer + readOffset, restData);
			if (bytesProcessed == 0) // It means that last command was not fit in to one packet fully
			{
				// In that case move rest data at the begin of the buffer to parse it with next section
				memmove(buffer, buffer + readOffset, restData);

				currentOffset = restData;
//				printf("Retain bytes in the buffer %zd\n", currentOffset);
				break;
			}

			readOffset += bytesProcessed;
		}

		if (mFinish)
			break;
	}

	printf("Task is FINISHED\n");
}

ssize_t ServerTask::parseCommand(uint8_t * data, size_t dataSize)
{
	if (dataSize == 0)
		return 0;

	uint8_t firstByte = data[0];
	switch (firstByte)
	{
	case Type::HEAD:
		if (sizeof(ProtocolHead) > dataSize)
			return 0;
		else
		{
			processHeader(reinterpret_cast<ProtocolHead *>(data));
			return sizeof(ProtocolHead);
		}
	case Type::DATA:
		if (sizeof(ProtocolData) > dataSize)
			return 0;
		else
		{
			processData(reinterpret_cast<ProtocolData *>(data));
			return sizeof(ProtocolData);
		}
	}

	printf("ServerTask::parseCommand Wrong first byte = %d\n", firstByte);
	return 0;
}

void ServerTask::processHeader(const ProtocolHead * ph)
{
//	printf("PROCESS HEAD:\n");
	mCommandCode = ph->commandCode;
	mCommandId = ph->commandId;
	switch (ph->commandCode)
	{
	case CommandCode::HELLO:
//		printf("\tHELLO\n");
		sendAnswer(true);
		break;
	case CommandCode::OK:
//		printf("\tOK\n");
		break;
	case CommandCode::FAIL:
//		printf("\tFAIL\n");
		break;
	case CommandCode::FILE_NAME:
//		printf("\tFILE_NAME\n");
		break;
	case CommandCode::FILE_DATA:
		mFileData.reset(new uint8_t[ph->totalDataSize]);
//		printf("\tFILE_DATA\n");
		break;
	case CommandCode::END:
//		printf("\tEND\n");
		mFinish = true;
		break;
	}
}

void ServerTask::processData(const ProtocolData * pd)
{
	if (mCommandId != pd->commandId)
	{
		sendAnswer(false);
		return;
	}

//	printf("PROCESS DATA:\n");
	switch (mCommandCode)
	{
	case CommandCode::HELLO:
//		printf("\tHELLO\n");
		break;
	case CommandCode::OK:
//		printf("\tOK\n");
		break;
	case CommandCode::FAIL:
//		printf("\tFAIL\n");
		break;
	case CommandCode::FILE_NAME:
		mFileName = (char *)pd->data;
		if (mPFile)
		{
			fclose(mPFile);
			mPFile = nullptr;
		}
//		printf("\tFILE_NAME \"%s\"\n", mFileName.c_str());
		break;
	case CommandCode::FILE_DATA:
//		printf("\tFILE_DATA\n");
		writeFileData(mFileName, &pd->data[0], pd->dataSize);
		break;
	case CommandCode::END:
//		printf("\tEND\n");
		mFinish = true;
		break;
	}

//	sendAnswer(true);
}

void ServerTask::writeFileData(const std::string & fileName, const uint8_t * data, ssize_t dataSize)
{
	if (fileName.empty())
		return;

	if (!mPFile)
	{
		std::stringstream ss;
		ss << "./" << mHomeDirectory << "/";
		ss << fileName;
		mPFile = fopen(ss.str().c_str(), "w");
		if (mPFile)
			printf("File created: %s\n", ss.str().c_str());
		else
		{
			printf("Unable to create file \"%s\"\n", ss.str().c_str());
			return;
		}
	}

	fwrite((const void *)data, dataSize, 1, mPFile);
//	printf("%zd bytes written to the file\n", dataSize);
}

void ServerTask::sendAnswer(bool success)
{
	ProtocolHead ph;
	ph.commandCode = success ? CommandCode::OK : CommandCode::FAIL;

	int bytesSent = write(mClientSocketId, &ph, sizeof(ph));
	if (bytesSent < 0)
		printf("ServerTask::sendAnswer Filed to send the answer\n");
}
