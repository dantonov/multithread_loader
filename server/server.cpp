//
//
//

#include "server.h"
#include "server_task.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <errno.h>

#include <protocol.h>

static const std::string sHomeDirectory("downloads");

Server::Server()
{
}

Server::~Server() throw()
{
	if (mSocketId > 0)
		close(mSocketId);
}

void Server::start()
{
	// Creating socket
	mSocketId = socket(AF_INET, SOCK_STREAM, 0);
	if (mSocketId < 0)
		printf("Server::start Error opening socket\n");

	// Preparing address structure for bind
	sockaddr_in addr;
	bzero((char *)&addr, sizeof(addr));
	int portNumber = 63449;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(portNumber);

	// Binding address to socket
	if (bind(mSocketId, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		printf("Server::start Failed to bind socket to address\n");
		return;
	}

	// Listening socket
	if (listen(mSocketId, 5) == -1)
	{
		printf("Socket is unavailable to listen\n");
		return;
	}

	routine();
}

void Server::stop()
{
}

void Server::startTask()
{
}

void Server::routine()
{
	while (true)
	{
		// Connection
		sockaddr_in clientAddr;
		socklen_t cliSize = sizeof(clientAddr);
		int clientSocId = accept(mSocketId, (struct sockaddr *)&clientAddr, &cliSize);
		if (clientSocId < 0)
		{
			printf("Bad client address connected\n");
			continue;
		}

		mThreads.push_back(std::thread(&Server::thread, this, clientSocId));
	}

	close(mSocketId);
}

void Server::thread(int clientSocketId)
{
	ServerTask task(clientSocketId, sHomeDirectory);
	task.start();
}

void Server::translateErrno()
{
	printf("Server::translateErrno ");
	switch (errno)
	{
	case EAGAIN:
		printf("EAGAIN\n");
		break;
	case EBADF:
		printf("EBADF\n");
		break;
	case EFAULT:
		printf("EFAULT\n");
		break;
	case EINTR:
		printf("EINTR\n");
		break;
	case EINVAL:
		printf("EINVAL\n");
		break;
	case EIO:
		printf("EIO\n");
		break;
	case EISDIR:
		printf("EISDIR\n");
		break;
	case ENOTCONN:
		printf("ENOTCONN\n");
		break;
	default:
		printf("UNKNOWN %d\n", errno);
	}
}
