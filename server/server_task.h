//
// ServerTask used to execute some job
//

#ifndef __SERVER_TASK_H__
#define __SERVER_TASK_H__

#include <string>
#include <memory>

struct ProtocolHead;
struct ProtocolData;

/// A Task of the server
class ServerTask
{
public:
	ServerTask(int clientSocketId, const std::string & homeDirectory);
	virtual ~ServerTask() throw();

	/// Start executing task
	virtual void start();

private:
	/// ID of client socket for that task
	int mClientSocketId = -1;
	/// Name of the file
	std::string mFileName;
	/// Current command code
	uint8_t mCommandCode = 0;
	/// Current command id
	uint64_t mCommandId = 0;
	/// Current file data
	std::unique_ptr<uint8_t[]> mFileData;
	/// Finish current task
	bool mFinish = false;
	/// File descriptor
	FILE * mPFile = nullptr;
	/// Directory in which files will be saved
	std::string mHomeDirectory;

	/// Parse command
	/// @return true if received end task command
	ssize_t parseCommand(uint8_t * data, size_t dataSize);
	/// Process header command
	void processHeader(const ProtocolHead * ph);
	/// Process protocol data
	void processData(const ProtocolData * pd);

	/// Write data to the file
	void writeFileData(const std::string & fileName, const uint8_t * data, ssize_t dataSize);

	/// Send ansver
	void sendAnswer(bool success);
};

#endif // __SERVER_TASK_H__