//
// Server that used to apply file transfer from client and out in in to home directory
//

#ifndef __SERVER_H__
#define __SERVER_H__

#include <map>
#include <vector>
#include <thread>
#include <mutex>

class ServerTask;

/// Main server entity that manages and creates task entinies which does some job
class Server
{
public:
	Server();
	~Server() throw();

	/// Start server routine
	void start();
	/// Step server routine
	void stop();

private:
	/// Socket ID
	int mSocketId = -1;
	/// Thread list
	/// Socket -> thread
	std::vector<std::thread> mThreads;
	/// Mutex to access thread map
	std::mutex mThreadMapMx;

	/// Start new server tasks
	void startTask();
	/// Thread function
	void routine();
	/// Function to call from thread
	void thread(int clientSocketId);

	/// Translate error code
	void translateErrno();
};

#endif // __SERVER_H__
