//
//
//

/// Max client buffer size
#define sMaxBufferSize 256

/// Enumeration for the data structure type
enum Type : uint8_t
{
	HEAD = 3,
	DATA = 10
};

#pragma pack(push, 1)
/// Struct that describes client - server interaction
struct ProtocolHead
{
	uint8_t type = HEAD; /// Type of the following data
	uint8_t commandCode = 0; /// A command code
	uint64_t commandId = 0; /// An unique identifier of a command
	uint64_t totalDataSize = 0; /// Size of total data(dataSize + dataSize + dataSize)(total file length) which must be received in following commands
};

/// Struct that contains data for the command
struct ProtocolData
{
	uint8_t type = DATA; /// Type of the following data
	uint8_t dataType = 0; /// One of the data types (BEGIN of CONTINUE)
	uint64_t commandId = 0; /// An unique command ID (same as in HEAD)
	uint64_t dataSize = 0; /// Size of the following data
	uint8_t data[64]; /// Some usefull data
};

#pragma pack(pop)

/// Available command codes
enum CommandCode : uint8_t
{
	HELLO = 1, /// Handshake
	OK = 2, /// Command receive success
	FAIL = 3, /// Failed execute command
	FILE_NAME = 4, /// Receive file name
	FILE_DATA = 5, /// Receive file data
	END = 6 /// End of task
};

/// Available data types
enum DataType : uint8_t
{
	BEGIN = 0, /// The first block of data
	CONTINUE = 1 /// Any other following block
};
