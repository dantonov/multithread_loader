//
//
//
#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <string>
#include <thread>
#include <map>
#include <vector>
#include <deque>

// Class that searches files and creates transfer tasks
class Client
{
public:
	Client(const std::string & serverAddress, int serverPort);
	~Client() throw();

	/// Search files and creates hreads for each file
	void run();

private:
	/// IP-address of the server
	std::string mServerIpAddress;
	/// Port of the server
	int mServerPort = -1;

	/// Queue of files
	std::deque<std::string> mFileNames;
	/// Mutex to access queue
	std::mutex mQueueMx;
	/// List of threads
	std::vector<std::thread> mThreads;

	/// Executes in thread
	void thread();
	/// Is a regular file
	bool isFile(const std::string & name);
};

#endif //__CLIENT_H__
