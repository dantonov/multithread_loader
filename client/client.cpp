//
//
//
#include "client.h"
#include "client_task.h"
#include <dirent.h>
#include <sys/stat.h>

static const int sMaxThreads = 4;

Client::Client(const std::string & serverAddress, int serverPort)
	: mServerIpAddress(serverAddress),
	  mServerPort(serverPort)
{
}

Client::~Client() throw()
{
}

void Client::run()
{
	DIR *dir = nullptr;
	dirent * directoryStruct = nullptr;
	if ((dir = opendir(".")) == nullptr)
		return;

	while ((directoryStruct = readdir(dir)) != nullptr)
	{
		if (isFile(directoryStruct->d_name))
			mFileNames.push_back(directoryStruct->d_name);
//			printf("%s\n", directoryStruct->d_name);
	}

	closedir(dir);

	while (mThreads.size() < sMaxThreads)
		mThreads.push_back(std::thread(&Client::thread, this));

	for (auto & thread : mThreads)
		thread.join();
}

void Client::thread()
{
	while (true)
	{
		mQueueMx.lock();
		if (mFileNames.empty())
		{
			mQueueMx.unlock();
			break;
		}

		auto fileName = mFileNames.front();
		printf("Sending \"%s\"\n", fileName.c_str());
		mFileNames.pop_front();
		mQueueMx.unlock();

		ClientTask ct(mServerIpAddress, mServerPort, fileName);
		ct.start();
	}

	printf("Client thread end\n");
}

bool Client::isFile(const std::string & name)
{
	struct stat path_stat;
	stat(name.c_str(), &path_stat);
	return S_ISREG(path_stat.st_mode);
}
