//
//
//

#include <unistd.h>

#include "client_task.h"
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#include <protocol.h>

ClientTask::ClientTask(const std::string & ipAddress, int port, const std::string & fileName)
	: mServerIpAddress(ipAddress),
	  mServerPort(port),
	  mFileName(fileName)
{
	mSocketId = socket(AF_INET, SOCK_STREAM, 0);
	if (mSocketId < 0)
		printf("Client: socket was not created\n");

	mFile = fopen(fileName.c_str(), "r");
	if (!mFile)
		printf("File cannot be opened for READ \"%s\"\n", fileName.c_str());
}

ClientTask::~ClientTask() throw()
{
	if (mSocketId != -1)
		close(mSocketId);
	if (mFile)
		fclose(mFile);
}

void ClientTask::start()
{
	if (mSocketId < 0)
		return;

	hostent * serverAddr = gethostbyname(mServerIpAddress.c_str());

	sockaddr_in server;
	bzero((char*) &server, sizeof(server));

	server.sin_family = AF_INET;
	bcopy((char *)serverAddr->h_addr, (char *)&server.sin_addr.s_addr, serverAddr->h_length);
	server.sin_port = htons(mServerPort);

	int connectStatus = connect(mSocketId, (sockaddr *) &server, sizeof(server));
	if (connectStatus < 0)
	{
		printf("Client::start connection to the server not available\n");
		return;
	}

	fseek(mFile, 0, SEEK_END);
	size_t dataSize = ftell(mFile);
	fseek(mFile, 0, SEEK_SET);

	sendFile(mFileName, dataSize);
}

void ClientTask::sendFile(const std::string & fileName, ssize_t fileSize)
{
	// Handshake
	ProtocolHead ph;
	ph.commandCode = CommandCode::HELLO;
	send((uint8_t *)&ph, sizeof(ph));

	// Send name of the file
	ph.commandCode = CommandCode::FILE_NAME;
	send((uint8_t *)&ph, sizeof(ph));

	ProtocolData pd;
	ssize_t fileNameSize = fileName.size();
	pd.dataSize = fileNameSize < sizeof(pd.data) ? fileNameSize : sizeof(pd.data);
	memcpy(pd.data, &fileName[0], pd.dataSize);
	send((uint8_t *)&pd, sizeof(pd));

	// Send file data
	ph.commandCode = CommandCode::FILE_DATA;
	send((uint8_t *)&ph, sizeof(ph));

	ssize_t offset = 0;
	while (offset < fileSize)
	{
		ssize_t retain = fileSize - offset;
		ssize_t blockSize = retain < sizeof(pd.data) ? retain : sizeof(pd.data);

		fread(pd.data, blockSize, 1, mFile);

		pd.dataSize = blockSize;
		send((uint8_t *)&pd, sizeof(pd));

		offset += blockSize;
	}

	// Send end of session
	ph.commandCode = CommandCode::END;
	send((uint8_t *)&ph, sizeof(ph));
}

void ClientTask::send(uint8_t * data, uint64_t dataSize)
{
	int bytesWrite = write(mSocketId, reinterpret_cast<void *>(data), dataSize);
	if (bytesWrite < 0)
		printf("Client::start write bytes to the server failed\n");
}
