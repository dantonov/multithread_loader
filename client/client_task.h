//
//
//

#ifndef __CLIENT_TASK_H__
#define __CLIENT_TASK_H__

#include <string>

/// Client for transfer some useful data
/// Connects to the corresponding server
class ClientTask
{
public:
	ClientTask(const std::string & ipAddress, int port, const std::string & fileName);
	~ClientTask() throw();

	/// Starts doing some job
	void start();

private:
	/// IP-address of the server
	std::string mServerIpAddress;
	/// Port of the server
	int mServerPort = -1;
	/// Socket for client
	int mSocketId = 0;
	/// A file descriptor
	FILE * mFile = nullptr;
	/// File name
	std::string mFileName;

	/// Process and send file
	void sendFile(const std::string & fileName, ssize_t fileSize);
	/// Send raw data to socket
	void send(uint8_t * data, uint64_t dataSize);
};

#endif // __CLIENT_TASK_H__
